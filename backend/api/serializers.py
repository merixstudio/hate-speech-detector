from rest_framework import serializers


class TweetInputSerializer(serializers.Serializer):
    tweet = serializers.CharField(required=True)

    class Meta:
        fields = ('tweet',)


class ReviewedTweetInputSerializer(serializers.Serializer):
    tweet = serializers.CharField(required=True)
    is_hate_speech = serializers.BooleanField(required=True)

    class Meta:
        fields = ('tweet', 'is_hate_speech',)
