import pandas as pd

from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from hate_speech_AI.core import classifier
from hate_speech_AI.utils import clean_and_tokenize_string
from .serializers import TweetInputSerializer, ReviewedTweetInputSerializer


class PredictHateSpeechView(GenericAPIView):
    serializer_class = TweetInputSerializer
    permissions = [AllowAny]

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            tweet = serializer.data.get('tweet')
            tweet = clean_and_tokenize_string(tweet)
            prediction = classifier.predict_single_tweet(tweet)
            return Response(
                {'is_hate_speech': bool(prediction)},
                status=status.HTTP_200_OK,
            )
        return Response(
            {'errors': serializer.errors},
            status=status.HTTP_400_BAD_REQUEST,
        )


class GetScoreView(APIView):
    permissions = [AllowAny]

    def get(self, request):
        score = classifier.get_score()
        return Response({'score': score}, status=status.HTTP_200_OK)


class AddReviewedTweetManuallyView(GenericAPIView):
    serializer_class = ReviewedTweetInputSerializer
    permissions = [AllowAny]

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            train_file = pd.read_csv('/code/hate_speech_AI/train.csv')
            tweet = serializer.data.get('tweet')

            is_hate_speech = serializer.data.get('is_hate_speech')
            label = 1 if is_hate_speech else 0

            id = len(train_file) + 1

            data_to_append = pd.DataFrame(
                [[id, label, tweet]],
                columns=['id', 'label', 'tweet'],
            )

            with open('/code/hate_speech_AI/train.csv', 'a') as f:
                data_to_append.to_csv(f, header=False, index=False)

            return Response(
                {'detail': 'Tweet added to the database correctly'},
                status=status.HTTP_200_OK,
            )
        return Response(
            {'errors': serializer.errors},
            status=status.HTTP_400_BAD_REQUEST,
        )
