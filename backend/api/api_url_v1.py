from django.urls import path

from .views import (
    PredictHateSpeechView,
    GetScoreView,
    AddReviewedTweetManuallyView,
)

api_urlpatterns = [
    path(
        'predict_tweet/',
        PredictHateSpeechView.as_view(),
        name='predict-tweet',
    ),
    path(
        'model_score/',
        GetScoreView.as_view(),
        name='model-score',
    ),
    path(
        'add_tweet/',
        AddReviewedTweetManuallyView.as_view(),
        name='add-tweet',
    ),
]
