import nltk
import pandas as pd
import seaborn as sns

from matplotlib import use as mpl_use
mpl_use('TkAgg')
# Import after telling matplotlib what backend to use. Mac OS error.
from matplotlib import pyplot as plt


def plot_frequent_hashtags(hashtags):
    b = nltk.FreqDist(hashtags)
    e = pd.DataFrame({'Hashtag': list(b.keys()), 'Count': list(b.values())})
    # selecting top 10 most frequent hashtags
    e = e.nlargest(columns="Count", n=10)
    plt.figure(figsize=(16, 5))
    ax = sns.barplot(data=e, x="Hashtag", y="Count")
    ax.set(ylabel='Count')
    plt.show()
