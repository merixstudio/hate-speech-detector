import pandas as pd

from matplotlib import use as mpl_use
from wordcloud import WordCloud
from utils import clean_and_tokenize_tweets
mpl_use('TkAgg')
# Import after telling matplotlib what backend to use. Mac OS error.
from matplotlib import pyplot as plt


def plot_wordcloud(combi, sentiment):
    # Sentiment can be 0 or 1. 0 = positive, 1 = negative
    words = ' '.join(
        [text for text in combi['tidy_tweet'][combi['label'] == sentiment]]
    )

    wordcloud = WordCloud(
        collocations=False,
        width=800,
        height=500,
        random_state=21,
        max_font_size=110,
    ).generate(words)

    plt.figure(figsize=(10, 7))
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis('off')
    plt.show()


train = pd.read_csv('train.csv')

train_data = clean_and_tokenize_tweets(train)

plot_wordcloud(train_data, 1)  # Hate speech words wordcloud
