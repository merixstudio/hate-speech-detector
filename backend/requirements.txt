celery==4.2.0
django-cors-headers==2.4.0
django>=2.1,<2.2
djangorestframework==3.8.2
django-redis==4.9.0
psycopg2==2.7.5
django-environ==0.4.5
colour==0.1.5
pillow==5.3.0
nltk==3.4
numpy==1.15.4
matplotlib==3.0.2
pandas==0.23.4
scipy==1.1.0
requests==2.19.1
redis==2.10.6
scikit-learn==0.20.0
stemming==1.0.1
progressbar2==3.38.0
uwsgi==2.0.17.1
wordcloud==1.5.0