from django.core.management.base import BaseCommand

from hate_speech.tasks import auto_first_train_model


class Command(BaseCommand):
    help = 'Runs first training'

    def handle(self, *args, **options):
        auto_first_train_model.delay()
