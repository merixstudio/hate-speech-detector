from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class HateSpeechConfig(AppConfig):
    name = 'hate_speech'
    verbose_name = _('Hate speech Backend')
