from celery import shared_task
from hate_speech_AI.core import classifier


@shared_task
def auto_first_train_model():
    classifier.train_model()
    print('Model trained')
